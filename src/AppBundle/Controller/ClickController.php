<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Click;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClickController extends Controller
{
    /**
     * @Route("/click/", name="click_action")
     */
    public function clickAction(Request $request)
    {
        $expectedParams = ['param1', 'param2'];
        $givenParams = [];
        foreach ($expectedParams as $expectedParam) {
            $givenParam = $request->query->get($expectedParam);
            if ($givenParam === null) {
                return new Response('Oh cmon. You shall not pass without params');
            }
            $givenParams[$expectedParam] = $givenParam;
        }
        $userAgent = $request->headers->get('User-Agent');
        $userIp = $request->getClientIp();
        $userReferer = $request->headers->get('Referer');

        $clickCreationalData = array_merge(
            [
                'ua' => $userAgent,
                'ip' => $userIp,
                'ref' => $userReferer
            ], $givenParams
        );

        $clickEntity = Click::createFromArray($clickCreationalData);

        $em = $this->container->get('doctrine.orm.entity_manager');


        $similarClick = $em->getRepository('AppBundle:Click')
            ->findSimilar($clickEntity, Click::$uniqueParamChain);

        if (empty($similarClick)) {
            $clickToSave = $clickEntity;
            $em->persist($clickToSave);
            $em->flush();

            $response =  $this->redirectToRoute('click_success', ['id' => $clickToSave->getId()]);
        } else {
            $clickToSave = reset($similarClick);
            $clickToSave->errorApproved();

            $response = $this->redirectToRoute('click_error', ['id' => $clickToSave->getId()]);
        }

        if (!empty($clickEntity->getRef())) {
            $parsedHost = parse_url($clickEntity->getRef())['host'];
            $badDomains = $em->getRepository('AppBundle:BadDomain')->findSimilarDomains($parsedHost);
            if (!empty($badDomains)) {
                $clickToSave->markDomain();
                $response = $this->redirectToRoute('click_error', ['id' => $clickToSave->getId()]);

            }
        }

        $em->persist($clickToSave);
        $em->flush();

        return $response;

    }

    /**
     * @Route("/success/{id}", name="click_success")
     */
    public function displaySuccessAction(Click $click)
    {
        return $this->render('click/success.html.twig', ['click' => $click]);
    }

    /**
     * @Route("/error/{id}", name="click_error")
     */
    public function displayErrorAction(Click $click)
    {
        return $this->render('click/error.html.twig', ['click' => $click]);
    }
}
