<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $clicks = $em->getRepository('AppBundle:Click')->findAll();
        $domains = $em->getRepository('AppBundle:BadDomain')->findAll();

        return $this->render('main.html.twig', ['clicks' => $clicks, 'domains' => $domains]);
    }
}
