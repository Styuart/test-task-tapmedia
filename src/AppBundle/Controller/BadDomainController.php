<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BadDomain;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BadDomainController extends Controller
{
    /**
     * @Route("/add_bad_domain", name="add_bad_domain")
     */
    public function addAction(Request $request)
    {
        $domainName = $request->request->get('name');
        if (empty($domainName)) {
            //Task is not about validation and errors.
            throw new \Exception('Someone please do validation');
        }
        $em = $this->container->get('doctrine.orm.entity_manager');

        $domainEntity = new BadDomain();
        $domainEntity->setName($domainName);
        $em->persist($domainEntity);
        $em->flush();
        
        return $this->redirectToRoute('homepage');
    }
}
