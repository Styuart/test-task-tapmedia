<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClickRepository")
 * @ORM\Table(name="click")
 */
class Click
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $ua;

    /**
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ref;

    /**
     * @ORM\Column(type="string")
     */
    private $param1;

    /**
     * @ORM\Column(type="string")
     */
    private $param2;

    /**
     * @ORM\Column(type="integer")
     */
    private $error = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $badDomain = false;

    public static $uniqueParamChain = ['ua', 'ip', 'ref', 'param1'];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUa()
    {
        return $this->ua;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @return mixed
     */
    public function getParam1()
    {
        return $this->param1;
    }

    /**
     * @return mixed
     */
    public function getParam2()
    {
        return $this->param2;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getBadDomain()
    {
        return $this->badDomain;
    }

    public static function createFromArray(array $creationalData)
    {
        $tempEntity = new self();
        $creationalFieldsAvailable = ['ua', 'ip', 'ref', 'param1', 'param2'];
        foreach ($creationalFieldsAvailable as $field) {
            if (!array_key_exists($field, $creationalData)) {
                throw new ValidatorException('Someone should do validation. Im rich model, but not enough');
            }
            $tempEntity->{$field} = $creationalData[$field];
        }

        return $tempEntity;
    }

    public function errorApproved()
    {
        $this->error += 1;

        return $this;
    }

    public function markDomain()
    {
        $this->badDomain = true;
        $this->errorApproved();

        return $this;
    }
}