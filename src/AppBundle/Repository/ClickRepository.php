<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.06.2018
 * Time: 15:47
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Click;
use Doctrine\ORM\EntityRepository;

class ClickRepository extends EntityRepository
{
    public function findSimilar(Click $targetEntity, array $similarParams)
    {
        $qb = $this->createQueryBuilder('click', 'click.id');
        foreach ($similarParams as $similarParam) {
            $qb->andWhere('click.' . $similarParam . ' = (:' . $similarParam .  ')' )
                ->setParameter($similarParam, $targetEntity->{'get'.$similarParam}());
        }
        $result = $qb->getQuery()->execute();
        return $result;
    }
}