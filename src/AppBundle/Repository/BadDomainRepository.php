<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.2018
 * Time: 15:33
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class BadDomainRepository extends EntityRepository
{

    public function findSimilarDomains($domainName)
    {
        $qb = $this->createQueryBuilder('bad_domain', 'bad_domain.id');
        $qb->Where('bad_domain.name LIKE :domain_name' )
            ->setParameter('domain_name', '%'.$domainName.'%');
        $result = $qb->getQuery()->execute();
        return $result;
    }
}