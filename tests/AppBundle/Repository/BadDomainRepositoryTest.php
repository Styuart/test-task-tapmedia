<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.2018
 * Time: 20:50
 */

namespace Tests\AppBundle\Repository;


use AppBundle\Entity\BadDomain;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BadDomainRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindSimilarDomains()
    {
        $entity = new BadDomain();
        $entity->setName('http://testdomain.com');
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $repoResult = $this->entityManager->getRepository('AppBundle:BadDomain')
            ->findSimilarDomains('testdomain.com');
        $this->assertTrue(!empty($repoResult));

        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}