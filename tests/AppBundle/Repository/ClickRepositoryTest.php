<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.2018
 * Time: 20:43
 */

namespace Tests\AppBundle\Repository;


use AppBundle\Entity\Click;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ClickRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindSimilar()
    {
        $creationalData = [
            'ua' => 'User agent',
            'ip' => '127.0.0.1',
            'ref' => 'google.com',
            'param1' => 'param1Value',
            'param2' => 'param2Value'
        ];
        $entity = Click::createFromArray($creationalData);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $repoResult = $this->entityManager->getRepository('AppBundle:Click')
            ->findSimilar($entity, Click::$uniqueParamChain);

        $this->assertTrue(!empty($repoResult));

        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}