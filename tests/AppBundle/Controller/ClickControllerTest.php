<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.2018
 * Time: 20:58
 */

namespace Tests\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClickControllerTest extends WebTestCase
{
    public function testClicks()
    {
        $client = static::createClient();
        $requestParams = [
                'HTTP_REFERER' => 'http://gooooogle' . time()  .  '.com/',
                'HTTP_USER_AGENT' => 'Mozilla/4.5 [en] (X11; U; Linux 2.2.9 i586)',
                'REMOTE_ADDR' => '127.0.0.1'
        ];
        $client->request(
            'get',
            '/click/?param1=123&param2=456',
            [],
            [],
            $requestParams
        );

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $crawler = $client->followRedirect();
        $this->assertContains('SUCCESS!', $crawler->filter('h3')->text());

        $client->restart();
        $client->request(
            'get',
            '/click/?param1=123&param2=456',
            [],
            [],
            $requestParams
        );
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $crawler = $client->followRedirect();
        $this->assertContains('ERROR!', $crawler->filter('h3')->text());
    }

    public function testClickBadDomain()
    {
        $client = static::createClient();
        $domain = 'http://baddomain' . time()  .  '.com/';
        $client->request('Post', '/add_bad_domain', ['name' => $domain]);
        $requestParams = [
            'HTTP_REFERER' => $domain,
            'HTTP_USER_AGENT' => 'Mozilla/4.5 [en] (X11; U; Linux 2.2.9 i586)',
            'REMOTE_ADDR' => '127.0.0.1'
        ];
        $client->restart();
        $client->request(
            'get',
            '/click/?param1=123&param2=456',
            [],
            [],
            $requestParams
        );
        $crawler = $client->followRedirect();
        $this->assertContains('Even more!', $crawler->filter('h2')->text());

    }
}