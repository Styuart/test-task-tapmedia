<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.2018
 * Time: 20:30
 */

namespace Tests\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BadDomainControllerTest extends WebTestCase
{
    public function testAdd()
    {
        $client = static::createClient();
        $client->request('Post', '/add_bad_domain', ['name' => 'testDomainAdd' . time()]);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}