<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.2018
 * Time: 20:01
 */

namespace Tests\AppBundle\Entity;


use AppBundle\Entity\Click;
use PHPUnit\Framework\TestCase;

class ClickTest extends TestCase
{

    public function testErrorApproved()
    {
        $entity = new Click();
        $entity->errorApproved();
        $this->assertEquals(1, $entity->getError());
    }

    public function testMarkDomain()
    {
        $entity = new Click();
        $entity->markDomain();
        $this->assertEquals(1, $entity->getError());
        $this->assertEquals(true, $entity->getBadDomain());

    }

    public function testCreateFromArray()
    {
        $creationalData = [
            'ua' => 'User agent',
            'ip' => '127.0.0.1',
            'ref' => 'google.com',
            'param1' => 'param1Value',
            'param2' => 'param2Value'
        ];
        $entity = Click::createFromArray($creationalData);
        $this->assertEquals('User agent', $entity->getUa());
        $this->assertEquals('127.0.0.1', $entity->getIp());
        $this->assertEquals('google.com', $entity->getRef());
        $this->assertEquals('param1Value', $entity->getParam1());
        $this->assertEquals('param2Value', $entity->getParam2());
        $this->assertEquals(0, $entity->getError());
        $this->assertEquals(false, $entity->getBadDomain());

    }
}